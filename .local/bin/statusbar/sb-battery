#!/usr/bin/env sh

######################################################################
# @author      : swytch (adapted from Luke Smith - lukesmith.xyz)
# @file        : sb-battery
# @license     : GPLv3
# @created     : Saturday Feb 13, 2021 17:19:04 CET
#
# @description : battery block dwmblocks
######################################################################


# Will show all batteries with approximate icon for remaining power.
# Or show that the computer is plugged to a power source
# In any case, show the remaining battery percentage
# Also sends a notification if battery running low

notify() { \
	now=$(date +%s)
	if [ -e $XDG_CONFIG_HOME/batteryupdate ]; then
		old=$(cat $XDG_CONFIG_HOME/batteryupdate)
		delta=$(expr $now - $old)
	else
		delta=$now
	fi
	[ 300 -gt $delta ] && return;
	echo $now > $XDG_CONFIG_HOME/batteryupdate
	case "$capacity" in
		1[0-9])	notify-send -u "normal" "Battery is running low ($capacity%)" "Please plug your computer to a power source" ;;
		[0-9])	notify-send -u "critical" "Battery is dangerously low ($capacity%)" "Please plug your computer to a power source - <b>NOW!</b>" ;;
	esac
	}

for bat in /sys/class/power_supply/BAT?/
do
	status="$(cat "$bat/status")"
	capacity="$(cat "$bat/capacity")"
	if [ "$status" = "Full" ]; then
		status=" " && capacity="FULL"
		printf " %s %s " "$status" "$capacity" && exit
 	elif [ "$status" = "Charging" ]; then
		status=""
	else
		case "$capacity" in
			100|[8-9][0-9])	status="" ;;
			[6-7][0-9])	status="" ;;
			[4-5][0-9])	status="" ;;
			[2-3][0-9])	status="" ;;
			*)	status="" ; notify;;
		esac
	fi
	printf " %s %3d%% " "$status" "$capacity";
done && return 0
